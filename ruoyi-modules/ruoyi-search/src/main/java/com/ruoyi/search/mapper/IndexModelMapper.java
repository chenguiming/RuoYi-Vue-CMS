package com.ruoyi.search.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.search.domain.IndexModel;

/**
 * <p>
 * 索引模型Mapper 接口
 * </p>
 *
 * @author 兮玥
 * @email liweiyimwz@126.com
 */
public interface IndexModelMapper extends BaseMapper<IndexModel> {

}