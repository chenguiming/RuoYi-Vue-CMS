package com.ruoyi.system.domain.vo;

/**
 * 存储方式
 * 
 * @author 兮玥
 * @email liweiyimwz@126.com
 */
public record StorageTypeVO (String id, String name) {

}
