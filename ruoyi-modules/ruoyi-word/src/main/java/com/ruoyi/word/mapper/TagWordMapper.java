package com.ruoyi.word.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.word.domain.TagWord;

/**
 * <p>
 * TAG词Mapper 接口
 * </p>
 *
 * @author 兮玥
 * @email liweiyimwz@126.com
 */
public interface TagWordMapper extends BaseMapper<TagWord> {

}
